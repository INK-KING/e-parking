var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var users = require('./routes/users');
var drivers = require('./routes/drivers');
var parking = require('./routes/parking');
var app = express();
var mongoose = require('mongoose');
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var uri = 'mongodb://renlong:Brq-6q6-aYh-Y2k@ds143362.mlab.com:43362/e-parking';
var connect = function () {
    mongoose.connect(uri);
};
connect();
var db = mongoose.connection;
db.on('error', function(error){
    console.log("Hello ")
    console.log("Error loading the db - "+ error);
});
// var mongoClient = require('mongodb').MongoClient;
// var uri = 'mongodb://renlong:Brq-6q6-aYh-Y2k@ds143362.mlab.com:43362/e-parking';
// mongoClient.connect(uri, function(err, db){
//   if (err) {
//     console.log(err);
//   }
// });
app.use('/', index);
app.use('/users', users);
app.use('/drivers', drivers);
app.use('/parking', parking);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
