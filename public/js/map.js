// /**
//  * Created by jasonyeh on 7/14/17.
//  */

var markers = [];
var markers2 =[];
var uniqueId = 1;
var curLat;
var curLng;

// var infoWindow1;

function initMap() {
    var mapOptions = {
        center: new google.maps.LatLng(37.336079,-121.880454),
        zoom: 16
    };

    $.get( "parking/getAllParkingSpaces")
        .done(function( data ) {
            for (var x in data) {
                markers2.push(data[x]);
            }

            for (var i = 0; i < markers2.length; i++) {
                // add Marker
                addMarker2(markers2[i]);
            }
        });


    var map = new google.maps.Map(document.getElementById("map"), mapOptions);

    var input = /** @type {!HTMLInputElement} */(
        document.getElementById('pac-input'));

    var types = document.getElementById('type-selector');
    // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    // map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

    var infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });


    autocomplete.addListener('place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
        }
        marker.setIcon(/** @type {google.maps.Icon} */({
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
        }));
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        var address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }

        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
        infowindow.open(map, marker);
    });

    // Sets a listener on a radio button to change the filter type on Places
    // Autocomplete.
    function setupClickListener(id, types) {
        var radioButton = document.getElementById(id);
        radioButton.addEventListener('click', function() {
            autocomplete.setTypes(types);
        });
    }

    setupClickListener('changetype-all', []);
    setupClickListener('changetype-address', ['address']);
    setupClickListener('changetype-establishment', ['establishment']);
    setupClickListener('changetype-geocode', ['geocode']);

    for (var i = 0; i < markers2.length; i++) {
        // add Marker
        addMarker2(markers2[i]);
    }

    function addMarker2(props) {
        var marker = new google.maps.Marker({
            position: {lat:Number(props.loc.coordinates[1]), lng: Number(props.loc.coordinates[0])},
            map: map,
            //icon: props.iconImage
        });

        if (props.status === 0) {
            marker.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png');
        } else if (props.status === 1) {
            marker.setIcon('http://maps.google.com/mapfiles/ms/icons/red-dot.png');
        } else if (props.status === 2) {
            marker.setIcon('http://maps.google.com/mapfiles/ms/icons/orange-dot.png');
        }


        // Check for content
        if (props.status != undefined || props.price || props.address || props.description) {
            console.log(props.status);
            var content = '<div id="iw_container">' +
                '<div class="iw_title">lat: ' + props.loc.coordinates[1] + '<br />' + 'lng: ' + props.loc.coordinates[0] + '</div>' +
                '<div class="iw_content">Price: ' + props.price + ' dollar / hour' + '<br />'
                + 'Address: ' + props.address + '<br />'
                + 'Description: ' + props.description + '<br /> '
                + '<input type = \'button\' value = \'Delete\' onclick = \'DeleteMarker(" + marker.id + ");\' value = \'Delete\' />' +'</div></div>';

            var infoWindow = new google.maps.InfoWindow({
                content: content
            });

            marker.addListener('click', function() {
                infoWindow.open(map, marker);
            });
        }
    }

    //Attach click event handler to the map.
    google.maps.event.addListener(map, 'click', function (event) {
        //Determine the location where the user has clicked.
        var location = event.latLng;

        //Create a marker and placed it on the map.
        var marker = new google.maps.Marker({
            position: location,
            map: map,
            draggable: true,
            mapTypeId: google.maps.MapTypeId.HYBRID
        });

        marker.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png');
        //Set unique id
        marker.id = uniqueId;
        uniqueId++;

        //Attach click event handler to the marker.
        google.maps.event.addListener(marker, "click", function (event) {
            var content = 'Latitude: ' + location.lat() + '<br />Longitude: ' + location.lng();
            curLat = location.lat();
            curLng = location.lng();

            content += "<br id=\"test\"/>Price: <input type=\"text\" size='3' id=\"newPrice\">";
            content += ' dollar / hour';
            content += "<br />Address: <input type=\"textarea\" id=\"newAddress\">";
            content += "<br />Description: <input type=\"textarea\" id=\"newDes\">";
            content += "<br /><input type=\"submit\" id=\"submitNew\" value=\"Submit\" onclick = 'addMarker();'>";
            content += "<br /><input type = 'button' value = 'Delete' onclick = 'DeleteMarker(" + marker.id + ");' value = 'Delete' />";

            var infoWindow = new google.maps.InfoWindow({
                content: content
            });
            infoWindow.open(map, marker);
        });

        //Add marker to the array.
        markers.push(marker);
    });


}

function addMarker() {
    var price = $('#newPrice').val();
    var address = $('#newAddress').val();
    var des = $('#newDes').val();

    $.post( "parking/newParkingPot",
        { lat: curLat, lng: curLng, price: price, address: address, des:des, governmentId: 1 })
        .done(function( data ) {
            console.log(data);
            // if (data.code != 200) {
            //     alert("error");
            // }
            $('#test').html("hello")

        });
}

function DeleteMarker(id) {
    //Find and remove the marker from the Array
    for (var i = 0; i < markers.length; i++) {
        if (markers[i].id == id) {
            //Remove the marker from Map
            markers[i].setMap(null);

            //Remove the marker from array.
            markers.splice(i, 1);
            return;
        }
    }
}



