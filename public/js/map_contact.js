function initMap() {

    //1. Basic maker
    var sjsu = {lat: 37.336079, lng: -121.880454};

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 16,
        center: sjsu
    });
    var marker = new google.maps.Marker({
        position: sjsu,
        map: map
    });
}