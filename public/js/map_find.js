var markers2 =[];

function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 37.336079, lng: -121.880454},
        zoom: 16
    });
    var infoWindow = new google.maps.InfoWindow({map: map});

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent("You're here");
            map.setCenter(pos);
        }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }

    $.get( "parking/getAllParkingSpaces")
        .done(function( data ) {
            for (var x in data) {
                markers2.push(data[x]);
            }

            for (var i = 0; i < markers2.length; i++) {
                // add Marker
                addMarker2(markers2[i]);
            }
        });


    function addMarker2(props) {
        var marker = new google.maps.Marker({
            position: {lat:Number(props.loc.coordinates[1]), lng: Number(props.loc.coordinates[0])},
            map: map,
            //icon: props.iconImage
        });

        if (props.status === 0) {
            marker.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png');
        } else if (props.status === 1) {
            marker.setIcon('http://maps.google.com/mapfiles/ms/icons/red-dot.png');
        } else if (props.status === 2) {
            marker.setIcon('http://maps.google.com/mapfiles/ms/icons/orange-dot.png');
        }


        // Check for content
        if (props.status != undefined || props.price || props.address || props.description) {
            console.log(props.status);
            var content =
                '<div class="iw_content">Price: ' + props.price + ' dollar / hour' + '<br />'
                + 'Address: ' + props.address + '<br />'
                + 'Description: ' + props.description +'</div></div>';

            var infoWindow = new google.maps.InfoWindow({
                content: content
            });

            marker.addListener('click', function() {
                infoWindow.open(map, marker);
            });
        }
    }

}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
}
