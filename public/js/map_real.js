
// setInterval("initMap()",5000);

// Arrays of markers
var markers = [];
var gmarkers1 = [];

function initMap() {
    var options = {
        zoom: 16,
        center: {lat: 37.336079, lng: -121.880454}
    };

    $.get( "parking/getAllParkingSpaces")
        .done(function( data ) {
            for (var x in data) {
                markers.push(data[x]);
                // gmarkers1.push(data[x]);
            }

            for (var i = 0; i < markers.length; i++) {
                // add Marker
                addMarker(markers[i]);
            }

            // for (var key, val in data){
            //     console.log(val);
            // }
            // if (data.code != 200) {
            //     alert("error");
            // }
        });

    // // New Map
    var map = new google.maps.Map(document.getElementById('map'), options);

    // Google Map Address Autocomplete
    var input = /** @type {!HTMLInputElement} */(
        document.getElementById('pac-input'));

    var types = document.getElementById('type-selector');
    // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    // map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

    var infowindow = new google.maps.InfoWindow();
    var marker2 = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });


    autocomplete.addListener('place_changed', function() {
        infowindow.close();
        marker2.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
        }
        marker2.setIcon(/** @type {google.maps.Icon} */({
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
        }));
        marker2.setPosition(place.geometry.location);
        marker2.setVisible(true);

        var address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }

        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
        infowindow.open(map, marker2);
    });

    // Sets a listener on a radio button to change the filter type on Places
    // Autocomplete.
    function setupClickListener(id, types) {
        var radioButton = document.getElementById(id);
        radioButton.addEventListener('click', function() {
            autocomplete.setTypes(types);
        });
    }

    setupClickListener('changetype-all', []);
    setupClickListener('changetype-address', ['address']);
    setupClickListener('changetype-establishment', ['establishment']);
    setupClickListener('changetype-geocode', ['geocode']);

    // Loop through markers
    for (var i = 0; i < markers.length; i++) {
        // add Marker
        addMarker(markers[i]);
    }

    for (var i = 0; i < markers.length; i++) {
        gmarkers1.push(markers[i]);
    }

    function addMarker(props) {
        var status = props.status;
        var marker = new google.maps.Marker({
            position: {lat:Number(props.loc.coordinates[1]), lng: Number(props.loc.coordinates[0])},
            map: map,
            status: status
        });

        if (props.status === 0) {
            marker.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png');
        } else if (props.status === 1) {
            marker.setIcon('http://maps.google.com/mapfiles/ms/icons/red-dot.png');
        } else if (props.status === 2) {
            marker.setIcon('http://maps.google.com/mapfiles/ms/icons/orange-dot.png');
        }


        // Check for content
        if (props.status != undefined || props.curDriver.fname || props.curDriver.lname || props.curDriver.plateNum) {
            console.log(props.status);
            var content = '<div id="iw_container">' +
                '<div class="iw_title">Status: ' + props.status + '</div>' +
                '<div class="iw_content">lat: ' + props.loc.coordinates[1] + '<br />' + 'lng: ' + props.loc.coordinates[0] + '<br />'
                + 'Driver ID: ' + props.curDriver.driverId + '<br />'
                + 'Driver: '+ props.curDriver.fname + ' ' + props.curDriver.lname + '<br />'
                + 'Plate Number: ' + props.curDriver.plateNum + '<br />'
                + 'Address: ' + props.address + '</div></div>';

            var infoWindow = new google.maps.InfoWindow({
                content: content
            });

            marker.addListener('click', function() {
                infoWindow.open(map, marker);
            });
        }

        gmarkers1.push(marker);
        console.log("gmarker: " + gmarkers1);


        filterMarkers = function (status) {
            for (i = 0; i < markers.length; i++) {
                var marker = gmarkers1[i];
                // If is same category or category not picked
                if (marker.status == status || status.length == 0) {
                    console.log(marker);
                    marker.setVisible(true);
                }
                // status don't match
                else {
                    marker.setVisible(false);
                }
            }
        }
    }
}


// function addMarker(props) {
//     var marker = new google.maps.Marker({
//         position: {lat:Number(props.loc.coordinates[1]), lng: Number(props.loc.coordinates[0])},
//         map: map,
//     });
//
//     if (props.status === 0) {
//         marker.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png');
//     } else if (props.status === 1) {
//         marker.setIcon('http://maps.google.com/mapfiles/ms/icons/red-dot.png');
//     } else if (props.status === 2) {
//         marker.setIcon('http://maps.google.com/mapfiles/ms/icons/orange-dot.png');
//     }
//
//
//     // Check for content
//     if (props.status != undefined || props.curDriver.fname || props.curDriver.lname || props.curDriver.plateNum || props.cameraID) {
//         console.log(props.status);
//         var content = '<div id="iw_container">' +
//             '<div class="iw_title">Status: ' + props.status + '</div>' +
//             '<div class="iw_content">CameraId: ' + props.cameraID + '<br />'
//             + 'lat: '+ props.loc.coordinates[1] + '<br />'
//             + 'lng: ' + props.loc.coordinates[0] + '<br />'
//             + 'Driver ID: ' + props.curDriver.driverId + '<br />'
//             + 'Driver: '+ props.curDriver.fname + ' ' + props.curDriver.lname + '<br />'
//             + 'Plate Number: ' + props.curDriver.plateNum + '<br />'
//             + 'Address: ' + props.address + '</div></div>';
//
//         var infoWindow = new google.maps.InfoWindow({
//             content: content
//         });
//
//         marker.addListener('click', function() {
//             infoWindow.open(map, marker);
//         });
//     }
// }

