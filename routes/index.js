var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/login', function(req, res, next) {
    res.render('login', { title: 'Express' });
});


// render about page
router.get('/about', function(req, res, next) {
    res.render('about', { title: 'Express' });
});

// render find car page
router.get('/findspace', function(req, res, next) {
    res.render('findspace', { title: 'Express' });
});

// render sell car page
router.get('/sellcar', function(req, res, next) {
    res.render('sellcar', { title: 'Express' });
});


// render Contacts page
router.get('/contact', function(req, res, next) {
    res.render('contact', { title: 'Express' });
});

// render dashboard page
router.get('/dashboard', function(req, res, next) {
    res.render('dashboard', { title: 'Express' });
});

// render dashboard_map page
router.get('/dashboard_map', function(req, res, next) {
    res.render('dashboard_map', { title: 'Express' });
});

// render dashboard_realtime.ejs page
router.get('/dashboard_real', function(req, res, next) {
    res.render('dashboard_real', { title: 'Express' });
});


// render userprofile page
router.get('/userprofile', function(req, res, next) {
    res.render('userprofile', { title: 'Express' });
});


// render logintest page
router.get('/logintest', function(req, res, next) {
    res.render('logintest', { title: 'Express' });
});


//render test
router.post('/test', function(req, res, next) {
    console.log(req.body.username);
    console.log(req.body.password);

    //res.render('test', { title: 'Express' });
});
module.exports = router;



