/**
 * Created by Tacq on 7/26/17.
 */
var express = require('express');
var mySqlPool = require('../mysql-db');
var router = express.Router();
// var mongoose = require('mongoose');
var NodeGeocoder = require('node-geocoder');
var options = {
    provider: 'google',
    // Optional depending on the providers
    httpAdapter: 'https', // Default
    apiKey: 'AIzaSyDLWnH58ycm-_m_n3gcZGc8DWOlWP9CuJA', // for Mapquest, OpenCage, Google Premier
    formatter: null         // 'gpx', 'string', ...
};

var geocoder = NodeGeocoder(options);

// bookingList: {driverid, startTime, endTime}
// alive:1 die 0
// var parkingSpace = new mongoose.Schema({
//     id: Number,
//     createTime: Date,
//     createrId: String,
//     loc: {
//         type: { type: String },
//         coordinates: [Number],
//     },
//     price: String,
//     address: String,
//     description: String,
//     status: Number,
//     curDriver: {driverId: String, fname: String, lname: String, plateNum: String},
//     bookingList:[],
//     alive: Number,
//     cameraId: Number
// });
//
// parkingSpace.index({ "loc": "2dsphere" });
//
// var ParkingSpace = mongoose.model('parkingSpace', parkingSpace);

var ParkingSpace = require("./parkingModel");

// driver page
router.get('/', function(req, res, next) {
    res.render('login', { title: 'Express' });
});

router.get('/getAllParkingSpaces', function(req, res, next) {
    ParkingSpace.find({}, function(err, doc){
        if(err){
            console.log("Something wrong when updating data!");
            res.send("error")
        }
        res.send(doc);
    });
});

router.get('/like', function(req, res, next) {
    var likeList = req.query.likeList;
    ParkingSpace.find({id: {$in: likeList}}, function(err, doc){
        if(err){
            console.log(err);
            res.send("error")
        }
        console.log(doc);
        res.send(doc);
    });
});

router.get('/searchParkingSpace', function(req, res, next) {
    var lat = req.query.lat;
    var lng = req.query.lng;
    var address = req.query.address;
    var maxRadius = 5 * 1609.34;
    var result = {};
    if (address != "") {
        geocoder.geocode(address, function(err, geoRes) {
            if (err) console.log(err);
            lat = geoRes[0].latitude;
            lng = geoRes[0].longitude;

            ParkingSpace.aggregate([
                    { "$geoNear": {
                        "near": {
                            "type": "Point",
                            "coordinates": [lng,lat]
                        },
                        "distanceField": "distance",
                        "spherical": true,
                        "maxDistance": maxRadius
                        }
                    }, {"$match":{alive: 1, status: 0}}
            ], function(err,data) {
                if (err) {
                    result.code = 400;
                    result.value = err;
                } else {
                    result.code = 200;
                    result.location = {lat: lat, lng: lng};
                    console.log(result.location);
                    result.value = data;
                }

                result = JSON.stringify(result);
                res.send(result);
            });
        });
    } else {
        ParkingSpace.aggregate([
            { "$geoNear": {
                "near": {
                    "type": "Point",
                    "coordinates": [Number(lng), Number(lat)]
                },
                "distanceField": "distance",
                "spherical": true,
                "maxDistance": maxRadius
            }
            }, {"$match":{alive: 1, status: 0}}
        ], function(err,data) {
            if (err) {
                result.code = 400;
                result.value = err;
            } else {
                result.code = 200;
                result.location = {lat: Number(lat), lng: Number(lng)};
                console.log(result.location);
                result.value = data;
            }

            result = JSON.stringify(result);
            res.send(result);
        });
    }
});

router.get('/searchWithNoId', function(req, res, next) {
    var lat = req.query.lat;
    var lng = req.query.lng;
    var address = req.query.address;
    var maxRadius = 5 * 1609.34;
    var result = {};
    if (lat == "" || lng == "") {
        geocoder.geocode(address, function(err, geoRes) {
            if (err) console.log(err);
            lat = geoRes[0].latitude;
            lng = geoRes[0].longitude;

            ParkingSpace.aggregate([
                { "$geoNear": {
                    "near": {
                        "type": "Point",
                        "coordinates": [lng,lat]
                    },
                    "distanceField": "distance",
                    "spherical": true,
                    "maxDistance": maxRadius
                }
                }, {"$match":{alive: 1, status: 0}}
            ], function(err,data) {
                if (err) res.send(err);
                else res.send(data);
            });
        });
    } else {
        ParkingSpace.aggregate([
            { "$geoNear": {
                "near": {
                    "type": "Point",
                    "coordinates": [Number(lng), Number(lat)]
                },
                "distanceField": "distance",
                "spherical": true,
                "maxDistance": maxRadius
            }
            }, {"$match":{alive: 1, status: 0}}
        ], function(err,data) {
            if (err) res.send(err);
            else res.send(data);
        });
    }



});

router.post('/newParkingPot', function(req, res, next) {
    var lat = req.body.lat;
    var lng = req.body.lng;
    var price = req.body.price;
    var address = req.body.address;
    var des = req.body.des;
    var governmentId = req.body.governmentId;
    var cameraId = req.body.cameraId;

    console.log(lat + ":" + lng);
    var result = {};
    ParkingSpace.count({}, function(err, cnt) {
        if (err) {
            result.code = "401";
            result.value = err;
            res.send(result);
        }

        var newParkingSpace = new ParkingSpace({
            id: cnt,
            createTime: new Date(),
            createrId: governmentId,
            loc: { "type": "Point", "coordinates": [ lng, lat ]},
            price: price,
            address: address,
            description: des,
            status: 0,
            curDriver: {driverId: "", fname: "", lname: "", plateNum: ""},
            bookingList:[],
            alive: 1,
            cameraId: cameraId
        });

        newParkingSpace.save(function(err) {
            if (err) {
                throw err;
                result.code = "401";
                result.value = err;
            } else {
                result.code = "200";
                result.value = "success";
            }
            res.send(result);
        });
    });

});


module.exports = router;
