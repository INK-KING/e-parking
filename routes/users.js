var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var crypto = require('crypto');
var algorithm = 'aes-256-ctr';
var passEncode = 'd6F3Efeq';

// type 0: city government, 1-driver
var user = new mongoose.Schema({
    id: Number,
    createTime: Date,
    email: String,
    password: String,
    fname: String,
    lname: String,
    licence: String,
    city: String,
    state: String,
    cars:[],
    reservations:[],
    like:[],
    balance: Number,
    cards:[],
    searchHistory:[],
    type: Number
});
var User = mongoose.model('user', user);


// var parkingSpace = new mongoose.Schema({
//     id: Number,
//     createTime: Date,
//     createrId: String,
//     loc: {
//         type: { type: String },
//         coordinates: [Number],
//     },
//     price: String,
//     address: String,
//     description: String,
//     status: Number,
//     curDriver: {driverId: String, fname: String, lname: String, plateNum: String},
//     bookingList:[],
//     alive: Number,
//     cameraId: Number
// });

var ParkingSpace = require("./parkingModel");
/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/autoCompleteAddress', function(req, res, next) {
    var request = require('request');
    var propertiesObject = {APIKey:"AIzaSyDLWnH58ycm-_m_n3gcZGc8DWOlWP9CuJA", Country: "US", Query: req.query.address};
    var url = "http://api.address-validator.net/api/search";
    request({url: url, qs: propertiesObject}, function(err, response, body) {
        if(err) {
            console.log(err);
            res.send("error");
        }
        res.send(response.body)
    });

});

router.post('/updateSearchHistory', function(req, res, next) {
    var uid = req.body.uid;
    var address = req.body.address;

    var result = {};
    User.findOneAndUpdate({id: uid}, {$pull:{searchHistory:{address: address}}}, {new: true}, function(err, doc){
        if(err){
            result.code = 400;
            result.value = err;

            result = JSON.stringify(result);
            res.send(result);
        } else {
            User.findOneAndUpdate({id: uid}, {$push:{searchHistory:{address: address, date: new Date()}}}, {new: true}, function(err, doc){
                if(err){
                    result.code = 400;
                    result.value = err;
                } else {
                    result.code = 200;
                    result.value = "success";
                }

                result = JSON.stringify(result);
                res.send(result);
            });
        }
    });

});

router.get('/searchHistory', function(req, res, next) {
    var uid = req.query.uid;

    // return top 3 search history

    User.find({id: uid}, function(err, doc){
        if(err){
            console.log(err);
            res.send("error")
        }
        var searchHistoryArray = doc[0].searchHistory;
        searchHistoryArray.sort(custom_sort);

        if (searchHistoryArray.length <= 3) {
            res.send(searchHistoryArray);
        } else {
            res.send(searchHistoryArray.splice(0, 3));
        }
    });
});

function custom_sort(a, b) {
    return  new Date(b.date).getTime() - new Date(a.date).getTime();
}

router.post('/makeReservation', function(req, res, next) {
    var uid = req.body.uid;
    var fname = req.body.fname;
    var lname = req.body.lname;
    var license = req.body.license;

    var spaceId = req.body.spaceId;
    var address = req.body.address;
    var des = req.body.des;
    var price = req.body.price;
    var lat = req.body.lat;
    var lng = req.body.lng;

    var from = req.body.from;
    var to = req.body.to;
    var start = Date.parse(from.substr(0, 19));
    var end = Date.parse(to.substr(0, 19));
    var startStr = req.body.startStr;
    var endStr = req.body.endStr;

    var result = {};
    var cur = new Date();

    User.findOneAndUpdate({id: uid}, {$push:{reservations:
                                                {   spaceId: spaceId,
                                                    start: start,
                                                    end: end,
                                                    date: cur,
                                                    address: address,
                                                    des: des,
                                                    price: price,
                                                    startStr: startStr,
                                                    endStr: endStr,
                                                    lat: Number(lat),
                                                    lng: Number(lng)
                                                }
    }}, {new: true}, function(err, doc){
        if(err){
            result.code = 400;
            result.value = err;
            result = JSON.stringify(result);
            res.send(result);
        } else {
            ParkingSpace.findOneAndUpdate({id: spaceId}, {$push:{bookingList:{uid: uid, fname: fname, lname: lname, license:license, start: start, end: end, date: cur, startStr: startStr, endStr: endStr}}}, {new: true}, function(err, doc){
                if(err){
                    result.code = 400;
                    result.value = err;
                } else {
                    result.code = 200;
                    result.value = "success";
                }

                result = JSON.stringify(result);
                res.send(result);
            });
        }
    });
});

router.post('/searchHistory', function(req, res, next) {
    var uid = req.body.uid;
    var address = req.body.address;

    var result = {};
    User.findOneAndUpdate({id: uid}, {$push:{searchHistory:{address: address, date: new Date()}}}, {new: true}, function(err, doc){
        if(err){
            result.code = 400;
            result.value = err;
        } else {
            result.code = 200;
            result.value = "success";
        }

        result = JSON.stringify(result);
        res.send(result);
    });

});

router.post('/newCard', function(req, res, next) {
    var uid = req.body.uid;
    var number = req.body.number;
    var exp = req.body.exp;
    var csc = req.body.csc;


    var result = {};
    User.findOneAndUpdate({id: uid}, {$push:{cards:{number:number, exp: exp, csc:csc}}}, {new: true}, function(err, doc){
        if(err){
            result.code = 400;
            result.value = err;
        } else {
            result.code = 200;
            result.value = "success";
        }

        result = JSON.stringify(result);
        res.send(result);
    });
});


router.post('/balance', function(req, res, next){
    var newVal = req.body.newVal;
    var uid = req.body.uid;

    var result = {};
    User.findOneAndUpdate({id: uid}, {$set:{balance: newVal}}, {new: true}, function(err, doc){
        if(err){
            result.code = 400;
            result.value = err;
        } else {
            result.code = 200;
            result.value = "success";
        }

        result = JSON.stringify(result);
        res.send(result);
    });
});

router.post('/deleteCard', function(req, res, next) {
    var number = req.body.number;
    var exp = req.body.exp;
    var csc = req.body.csc;
    var uid = req.body.uid;

    var result = {};
    User.findOneAndUpdate({id: uid}, {$pull:{cards:{number:number, exp: exp, csc:csc}}}, {new: true}, function(err, doc){
        if(err){
            result.code = 400;
            result.value = err;
        } else {
            result.code = 200;
            result.value = "success";
        }

        result = JSON.stringify(result);
        res.send(result);
    });
});

router.post('/newCar', function(req, res, next) {
    var carModel = req.body.carModel;
    var plateNumber = req.body.plateNumber;
    var uid = req.body.uid;

    console.log(uid);

    var result = {};
    User.findOneAndUpdate({id: uid}, {$push:{cars:{model:carModel, plateNumber: plateNumber}}}, {new: true}, function(err, doc){
        if(err){
            result.code = 400;
            result.value = err;
        } else {
            result.code = 200;
            result.value = "success";
        }

        result = JSON.stringify(result);
        res.send(result);
    });
});

router.get('/newLicense', function(req, res, next){
    var newVal = req.query.newVal;
    var uid = req.query.uid;

    var result = {};
    User.findOneAndUpdate({id: uid}, {$set:{licence:newVal}}, {new: true}, function(err, doc){
        if(err){
            result.code = 400;
            result.value = err;
        } else {
            result.code = 200;
            result.value = "success";
        }

        result = JSON.stringify(result);
        res.send(result);
    });
});

router.get('/newFname', function(req, res, next){
    var newVal = req.query.newVal;
    var uid = req.query.uid;

    var result = {};
    User.findOneAndUpdate({id: uid}, {$set:{fname: newVal}}, {new: true}, function(err, doc){
        if(err){
            result.code = 400;
            result.value = err;
        } else {
            result.code = 200;
            result.value = "success";
        }

        result = JSON.stringify(result);
        res.send(result);
    });
});

router.get('/newLname', function(req, res, next){
    var newVal = req.query.newVal;
    var uid = req.query.uid;

    var result = {};
    User.findOneAndUpdate({id: uid}, {$set:{lname: newVal}}, {new: true}, function(err, doc){
        if(err){
            result.code = 400;
            result.value = err;
        } else {
            result.code = 200;
            result.value = "success";
        }

        result = JSON.stringify(result);
        res.send(result);
    });
});

router.post('/deleteCar', function(req, res, next) {
    var carModel = req.body.carModel;
    var plateNumber = req.body.plateNumber;
    var uid = req.body.uid;

    console.log(uid);

    var result = {};
    User.findOneAndUpdate({id: uid}, {$pull:{cars:{model:carModel, plateNumber: plateNumber}}}, {new: true}, function(err, doc){
        if(err){
            result.code = 400;
            result.value = err;
        } else {
            result.code = 200;
            result.value = "success";
        }

        result = JSON.stringify(result);
        res.send(result);
    });
});


router.get('/myCars', function(req, res, next) {
   var uid = req.query.uid;

   var result = {}
    User.find({id: uid}).exec(function(err, all) {
        if (err) {
            result.code = 401;
            result.value = err;
        } else {
            if (all.length == 1) {
                result.code = 200;
                result.value = all[0].cars;
            } else {
                result.code = 401;
                result.value = "User not exist";
            }
        }
        result = JSON.stringify(result);
        res.send(result);
    });

});

// get my reservations
router.get('/myReservations', function(req, res, next) {
    var uid = req.query.uid;

    var result = {}
    User.find({id: uid}).exec(function(err, all) {
        if (err) {
            result.code = 401;
            result.value = err;
        } else {
            if (all.length == 1) {
                result.code = 200;
                result.value = all[0].reservations;
            } else {
                result.code = 401;
                result.value = "User not exist";
            }
        }
        result = JSON.stringify(result);
        res.send(result);
    });

});

// get my cards info
router.get('/myCards', function(req, res, next) {
    var uid = req.query.uid;

    var result = {}
    User.find({id: uid}).exec(function(err, all) {
        if (err) {
            result.code = 401;
            result.value = err;
        } else {
            if (all.length == 1) {
                result.code = 200;
                result.value = all[0].cards;
            } else {
                result.code = 401;
                result.value = "User not exist";
            }
        }
        result = JSON.stringify(result);
        res.send(result);
    });

});


router.post('/login', function(req, res, next) {
    var email = req.body.email;
    var password = req.body.password;
    var type = req.body.type;
    console.log("Helll")
    console.log(email);
    console.log(password);
    console.log(type);

    var cipher = crypto.createCipher(algorithm, passEncode);
    var crypted = cipher.update(password, 'utf8', 'hex');
    crypted += cipher.final('hex');

    User.find({$and: [{email:email}, {password: crypted},{type: type}]}).exec(function(err, all) {
        if (err) {
            throw err;
            res.send(err);
            result.value = err;
        } else {
            console.log(all)
            if (all.length == 1) {
                console.log("tes")
                res.render('dashboard', {});
            } else {
                res.render('error', {});
            }
        }
    });
});

router.get('/like', function(req, res, next) {
    var uid = req.query.uid;
    var spaceId = req.query.spaceId;
    console.log(uid + ": " + spaceId);

    var result = {};
    User.findOneAndUpdate({id: uid}, {$push:{like:{spaceId:spaceId, date: new Date()}}}, {new: true}, function(err, doc){
        if(err){
            result.code = 400;
            result.value = err;
        } else {
            result.code = 200;
            result.value = "success";
        }

        result = JSON.stringify(result);
        res.send(result);
    });

});

router.get('/unlike', function(req, res, next) {
    var uid = req.query.uid;
    var spaceId = req.query.spaceId;

    var result = {};
    User.findOneAndUpdate({id: uid}, {$pull:{like:{spaceId:spaceId}}}, {new: true}, function(err, doc){
        if(err){
            result.code = 400;
            result.value = err;
        } else {
            result.code = 200;
            result.value = "success";
        }

        result = JSON.stringify(result);
        res.send(result);
    });
});

router.get('/login', function(req, res, next) {
   var email = req.query.email;
   var password = req.query.password;
   var type = req.query.type;

   var cipher = crypto.createCipher(algorithm, passEncode);
   var crypted = cipher.update(password, 'utf8', 'hex');
   crypted += cipher.final('hex');

    var result = {}
    User.find({$and: [{email:email}, {password: crypted},{type: type}]}).exec(function(err, all) {
        if (err) {
            throw err;
            result.code = 401;
            result.value = err;
        } else {
            if (all.length == 1) {
                result.code = 200;
                result.value = all[0];
            } else {
                result.code = 401;
                result.value = "User not exist";
            }
        }
        result = JSON.stringify(result);
        res.send(result);
    });
});

router.post('/signin', function(req, res, next) {
    var email = req.body.email;
    var password = req.body.password;
    var fname = req.body.fname;
    var lname = req.body.lname;
    var city = req.body.city;
    var state = req.body.state;
    var type = req.body.type;

    var cipher = crypto.createCipher(algorithm, passEncode);
    var crypted = cipher.update(password, 'utf8', 'hex');
    crypted += cipher.final('hex');

    var result = {};
    User.count({}, function(err, cnt) {
        if (err) {
            result.code = "401";
            result.value = err;
            res.send(result);
        }

        var newUser = new User({
            id: cnt,
            createTime: new Date(),
            email: email,
            password: crypted,
            fname: fname,
            lname: lname,
            licence: "",
            city: city,
            state: state,
            cars: [],
            bookingList:[],
            reservations: [],
            like: [],
            balance: 0,
            cards:[],
            searchHistory:[],
            type: type
        });

        newUser.save(function(err) {
            if (err) {
                throw err;
                result.code = "401";
                result.value = err;
            } else {
                result.code = "200";
                result.value = "success";
            }
            res.send(result);
        });
    });
});


module.exports = router;
